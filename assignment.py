import pandas as pd
import pymongo
xl_file = pd.read_excel('Sample Data.xlsx',0)
category = xl_file['CategoryOrder']

category = category.str.replace(r'\W'," ", regex=True)
xl_file[['Level0', 'Level1',  'Level2', 'Level3',  'Level4', 'level5']] = category.str.split('    ', expand=True)
xl_file.drop(['Level0', 'CategoryOrder'], axis=1, inplace=True)
xl_file = pd.DataFrame(xl_file, columns=xl_file.columns)

for i in xl_file['BrandName']:
 xl_file['ProductName'] = xl_file['ProductName'].str.replace(str(i), " ", regex=True)

xl_file = xl_file[xl_file.ProductName.notnull()]
xl_file.to_excel("finalsample.xlsx", index=False)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient['sampledata1']
mycollection = mydb['assesment1']
ff = pd.read_excel('finalsample.xlsx')
data = ff.to_dict(orient='records')
mycollection.insert_many(data)

results = list(xl_file[xl_file.columns].count())
for num in range(len(results)):
    print(f"Count based on {xl_file.columns[num]} category is ", results[num])

